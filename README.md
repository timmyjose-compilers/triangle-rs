# triangle-rs

An implementation of the [Triangle Programming Language](http://www.dcs.gla.ac.uk/~daw/books/PLPJ/), in Rust.

## LICENCE

See [LICENCE](LICENSE.md).